"""
    Fichier : gestion_avoir_sim_pc_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les sim.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *

"""
    Nom : films_genres_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /films_genres_afficher
    
    But : Afficher les films avec les sim associés pour chaque film.
    
    Paramètres : id_genre_sel = 0 >> tous les films.
                 id_genre_sel = "n" affiche le film dont l'id est "n"
                 
"""


@obj_mon_application.route("/avoir_sim_pc_afficher/<int:ID_PC_sel>", methods=['GET', 'POST'])
def avoir_sim_pc_afficher(ID_PC_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_avoir_sim_pc_afficher:
                code, msg = Exception_init_avoir_sim_pc_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_avoir_sim_pc_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_avoir_sim_pc_afficher.args[0]} , "
                      f"{Exception_init_avoir_sim_pc_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_avoir_sim_pc_afficher_data = """SELECT t_pc.ID_PC,t_pc.Modele_PC, t_pc.Numero_de_serie_PC, t_sim.ID_Sim,
                                                        t_sim.Modele, t_sim.numero_de_serie, t_sim.Date_achat,
                                                        t_sim.Pin1, t_sim.Pin2, t_sim.numero_de_telephone,
                                                        GROUP_CONCAT(Modele) as avoir_sim_pc FROM t_avoir_sim_pc
                                                        RIGHT JOIN t_pc ON t_pc.ID_PC = t_avoir_sim_pc.FK_PC
                                                        LEFT JOIN t_sim ON t_sim.ID_Sim = t_avoir_sim_pc.FK_Sim
                                                        GROUP BY ID_PC"""
                if ID_PC_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_avoir_sim_pc_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_ID_PC_selected_dictionnaire = {"value_ID_PC_selected": ID_PC_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_avoir_sim_pc_afficher_data += """ HAVING ID_PC= %(value_ID_PC_selected)s"""

                    mc_afficher.execute(strsql_avoir_sim_pc_afficher_data, valeur_ID_PC_selected_dictionnaire)

                # Récupère les données de la requête.
                data_avoir_sim_pc_afficher = mc_afficher.fetchall()
                print("data_sim ", data_avoir_sim_pc_afficher, " Type : ", type(data_avoir_sim_pc_afficher))

                # Différencier les messages.
                if not data_avoir_sim_pc_afficher and ID_PC_sel == 0:
                    flash("""La table "t_pc" est vide. !""", "warning")
                elif not data_avoir_sim_pc_afficher and ID_PC_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun film
                    flash(f"Le PC {ID_PC_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données PC et sim affichés !!", "success")

        except Exception as Exception_avoir_sim_pc_afficher:
            code, msg = Exception_avoir_sim_pc_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception avoir_sim_pc_afficher : {sys.exc_info()[0]} "
                  f"{Exception_avoir_sim_pc_afficher.args[0]} , "
                  f"{Exception_avoir_sim_pc_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("avoir_sim_pc/avoir_sim_pc_afficher.html", data=data_avoir_sim_pc_afficher)


"""
    nom: edit_avoir_sim_pc_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les sim du pc sélectionné par le bouton "MODIFIER" de "avoir_sim_pc_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les sim contenus dans la "t_sim".
    2) Les sim attribués au pc selectionné.
    3) Les sim non-attribués au pc sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_avoir_sim_pc_selected", methods=['GET', 'POST'])
def edit_avoir_sim_pc_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_sim_afficher = """SELECT * FROM t_sim ORDER BY ID_Sim ASC"""
                mc_afficher.execute(strsql_sim_afficher)
            data_sim_all = mc_afficher.fetchall()
            print("dans edit_avoir_sim_pc_selected ---> data_sim_all", data_sim_all)

            # Récupère la valeur de "id_film" du formulaire html "avoir_sim_pc_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "ID_Avoir_Sim_PC_edit_html" dans le fichier "avoir_sim_pc_afficher.html"
            # href="{{ url_for('edit_genre_film_selected', ID_Avoir_Sim_PC_edit_html=row.id_film) }}"
            ID_avoir_sim_pc_edit = request.values['ID_avoir_sim_pc_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_ID_avoir_sim_pc_edit'] = ID_avoir_sim_pc_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_ID_PC_selected_dictionnaire = {"value_ID_PC_selected": ID_avoir_sim_pc_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction genres_films_afficher_data
            # 1) Sélection du film choisi
            # 2) Sélection des sim "déjà" attribués pour le film.
            # 3) Sélection des sim "pas encore" attribués pour le film choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "genres_films_afficher_data"
            data_avoir_sim_pc_selected, data_avoir_sim_pc_non_attribues, data_avoir_sim_pc_attribues = \
                avoir_sim_pc_afficher_data(valeur_ID_PC_selected_dictionnaire)

            print(data_avoir_sim_pc_selected)
            lst_data_pc_selected = [item['ID_PC'] for item in data_avoir_sim_pc_selected]
            print("lst_data_pc_selected  ", lst_data_pc_selected,
                  type(lst_data_pc_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les sim qui ne sont pas encore sélectionnés.
            lst_data_avoir_sim_pc_non_attribues = [item['ID_Sim'] for item in data_avoir_sim_pc_non_attribues]
            session['session_lst_data_avoir_sim_pc_non_attribues'] = lst_data_avoir_sim_pc_non_attribues
            print("lst_data_avoir_sim_pc_non_attribues  ", lst_data_avoir_sim_pc_non_attribues,
                  type(lst_data_avoir_sim_pc_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les sim qui sont déjà sélectionnés.
            lst_data_avoir_sim_pc_old_attribues = [item['ID_Sim'] for item in data_avoir_sim_pc_attribues]
            session['session_lst_data_avoir_sim_pc_old_attribues'] = lst_data_avoir_sim_pc_old_attribues
            print("lst_data_avoir_sim_pc_old_attribues  ", lst_data_avoir_sim_pc_old_attribues,
                  type(lst_data_avoir_sim_pc_old_attribues))

            print(" data data_avoir_sim_pc_selected", data_avoir_sim_pc_selected, "type ", type(data_avoir_sim_pc_selected))
            print(" data data_avoir_sim_pc_non_attribues ", data_avoir_sim_pc_non_attribues, "type ",
                  type(data_avoir_sim_pc_non_attribues))
            print(" data_avoir_sim_pc_attribues ", data_avoir_sim_pc_attribues, "type ",
                  type(data_avoir_sim_pc_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_avoir_sim_pc_non_attribues = [item['Modele'] for item in data_avoir_sim_pc_non_attribues]
            print("lst_all_sim gf_edit_avoir_sim_pc_selected ", lst_data_avoir_sim_pc_non_attribues,
                  type(lst_data_avoir_sim_pc_non_attribues))

        except Exception as Exception_edit_avoir_sim_pc_selected:
            code, msg = Exception_edit_avoir_sim_pc_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_avoir_sim_pcn_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_avoir_sim_pc_selected.args[0]} , "
                  f"{Exception_edit_avoir_sim_pc_selected}", "danger")

    return render_template("avoir_sim_pc/avoir_sim_pc_modifier_tags_dropbox.html",
                           data_sim=data_sim_all,
                           data_pc_selected=data_avoir_sim_pc_selected,
                           data_sim_attribues=data_avoir_sim_pc_attribues,
                           data_sim_non_attribues=data_avoir_sim_pc_non_attribues)


"""
    nom: update_genre_film_selected

    Récupère la liste de tous les sim du film sélectionné par le bouton "MODIFIER" de "avoir_sim_pc_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les sim contenus dans la "t_genre".
    2) Les sim attribués au film selectionné.
    3) Les sim non-attribués au film sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_avoir_sim_pc_selected", methods=['GET', 'POST'])
def update_avoir_sim_pc_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            ID_PC_selected = session['session_ID_avoir_sim_pc_edit']
            print("session['session_ID_avoir_sim_pc_edit'] ", session['session_ID_avoir_sim_pc_edit'])

            # Récupère la liste des sim qui ne sont pas associés au film sélectionné.
            old_lst_data_avoir_sim_pc_non_attribues = session['session_lst_data_avoir_sim_pc_non_attribues']
            print("old_lst_data_avoir_sim_pc_non_attribues ", old_lst_data_avoir_sim_pc_non_attribues)

            # Récupère la liste des sim qui sont associés au film sélectionné.
            old_lst_data_avoir_sim_pc_attribues = session['session_lst_data_avoir_sim_pc_old_attribues']
            print("old_lst_data_avoir_sim_pc_old_attribues ", old_lst_data_avoir_sim_pc_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme sim dans le composant "tags-selector-tagselect"
            # dans le fichier "genres_films_modifier_tags_dropbox.html"
            new_lst_str_avoir_sim_pc = request.form.getlist('name_select_tags')
            print("new_lst_str_avoir_sim_pc ", new_lst_str_avoir_sim_pc)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_avoir_sim_pc_old = list(map(int, new_lst_str_avoir_sim_pc))
            print("new_lst_avoir_sim_pc ", new_lst_int_avoir_sim_pc_old, "type new_lst_avoir_sim_pc ",
                  type(new_lst_int_avoir_sim_pc_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genre_film".
            lst_diff_sim_delete_b = list(
                set(old_lst_data_avoir_sim_pc_attribues) - set(new_lst_int_avoir_sim_pc_old))
            print("lst_diff_sim_delete_b ", lst_diff_sim_delete_b)

            # Une liste de "id_genre" qui doivent être ajoutés à la "t_genre_film"
            lst_diff_sim_insert_a = list(
                set(new_lst_int_avoir_sim_pc_old) - set(old_lst_data_avoir_sim_pc_attribues))
            print("lst_diff_sim_insert_a ", lst_diff_sim_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_film"/"id_film" et "fk_genre"/"id_genre" dans la "t_genre_film"
            strsql_insert_avoir_sim_pc = """INSERT INTO t_avoir_sim_pc (ID_Avoir_Sim_PC, FK_Sim, FK_PC)
                                                    VALUES (NULL, %(value_FK_Sim)s, %(value_FK_PC)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_avoir_sim_pc = """DELETE FROM t_avoir_sim_pc WHERE FK_Sim = %(value_FK_Sim)s AND FK_PC = %(value_FK_PC)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le film sélectionné, parcourir la liste des sim à INSÉRER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for ID_sim_ins in lst_diff_sim_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
                    valeurs_pc_sel_sim_sel_dictionnaire = {"value_FK_PC": ID_PC_selected,
                                                               "value_FK_Sim": ID_sim_ins}

                    mconn_bd.mabd_execute(strsql_insert_avoir_sim_pc, valeurs_pc_sel_sim_sel_dictionnaire)

                # Pour le film sélectionné, parcourir la liste des sim à EFFACER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for ID_Sim_del in lst_diff_sim_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "id_genre_del" (l'id du genre dans la liste) associé à une variable.
                    valeurs_pc_sel_sim_sel_dictionnaire = {"value_FK_PC": ID_PC_selected,
                                                               "value_FK_Sim": ID_Sim_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_avoir_sim_pc, valeurs_pc_sel_sim_sel_dictionnaire)

        except Exception as Exception_update_avoir_sim_pc_selected:
            code, msg = Exception_update_avoir_sim_pc_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_avoir_sim_pc_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_avoir_sim_pc_selected.args[0]} , "
                  f"{Exception_update_avoir_sim_pc_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_genre_film",
    # on affiche les films et le(urs) genre(s) associé(s).
    return redirect(url_for('avoir_sim_pc_afficher', ID_PC_sel=ID_PC_selected))


"""
    nom: genres_films_afficher_data

    Récupère la liste de tous les sim du film sélectionné par le bouton "MODIFIER" de "avoir_sim_pc_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des sim, ainsi l'utilisateur voit les sim à disposition

    On signale les erreurs importantes
"""


def avoir_sim_pc_afficher_data(valeur_id_pc_selected_dict):
    print("valeur_ID_PC_selected_dict...", valeur_id_pc_selected_dict)
    try:

        strsql_pc_selected = """SELECT ID_PC, Modele_PC, Numero_de_serie_PC, GROUP_CONCAT(ID_Sim) as SimPc FROM t_avoir_sim_pc
                                        INNER JOIN t_pc ON t_pc.ID_PC = t_avoir_sim_pc.FK_PC
                                        INNER JOIN t_sim ON t_sim.ID_Sim = t_avoir_sim_pc.FK_Sim
                                        WHERE ID_PC = %(value_ID_PC_selected)s"""

        strsql_avoir_sim_pc_non_attribues = """SELECT * FROM t_sim WHERE ID_Sim not in(SELECT ID_Sim as idSimPc FROM t_avoir_sim_pc
                                                    INNER JOIN t_pc ON t_pc.ID_PC = t_avoir_sim_pc.FK_PC
                                                    INNER JOIN t_sim ON t_sim.ID_Sim = t_avoir_sim_pc.FK_Sim
                                                    WHERE ID_PC = %(value_ID_PC_selected)s)"""

        strsql_avoir_sim_pc_attribues = """SELECT * FROM t_avoir_sim_pc
                                            INNER JOIN t_pc ON t_pc.ID_PC = t_avoir_sim_pc.FK_PC
                                            INNER JOIN t_sim ON t_sim.ID_Sim = t_avoir_sim_pc.FK_Sim
                                            WHERE ID_PC = %(value_ID_PC_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_avoir_sim_pc_non_attribues, valeur_id_pc_selected_dict)
            # Récupère les données de la requête.
            data_avoir_sim_pc_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("avoir_sim_pc_afficher_data ----> data_avoir_sim_pc_non_attribues ", data_avoir_sim_pc_non_attribues,
                  " Type : ",
                  type(data_avoir_sim_pc_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_pc_selected, valeur_id_pc_selected_dict)
            # Récupère les données de la requête.
            data_pc_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_pc_selected  ", data_pc_selected, " Type : ", type(data_pc_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_avoir_sim_pc_attribues, valeur_id_pc_selected_dict)
            # Récupère les données de la requête.
            data_avoir_sim_pc_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_avoir_sim_pc_attribues ", data_avoir_sim_pc_attribues, " Type : ",
                  type(data_avoir_sim_pc_attribues))

            # Retourne les données des "SELECT"
            return data_pc_selected, data_avoir_sim_pc_non_attribues, data_avoir_sim_pc_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans avoir_sim_pc_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans avoir_sim_pc_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_avoir_sim_pc_afficher_data:
        code, msg = IntegrityError_avoir_sim_pc_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans avoir_sim_pc_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_avoir_sim_pc_afficher_data.args[0]} , "
              f"{IntegrityError_avoir_sim_pc_afficher_data}", "danger")
