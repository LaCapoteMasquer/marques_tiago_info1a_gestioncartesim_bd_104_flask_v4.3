"""
    Fichier : gestion_sim_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les sim.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.telephone.gestion_telephone_wtf_forms import FormWTFAjouterTelephone
from APP_FILMS.telephone.gestion_telephone_wtf_forms import FormWTFDeleteTelephone
from APP_FILMS.telephone.gestion_telephone_wtf_forms import FormWTFUpdateTelephone

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /telephone_afficher

    Test : ex : http://127.0.0.1:5005/telephone_afficher

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_telephone_sel = 0 >> tous les pc(s).
                ID_telephone_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/telephone_afficher/<string:order_by>/<int:ID_telephone_sel>", methods=['GET', 'POST'])
def telephone_afficher(order_by, ID_telephone_sel):
    # def telephone_afficher(order_by, ID_telephone_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion telephone ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestiontelephone {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_telephone_sel == 0:
                    strsql_telephone_afficher = """SELECT * FROM t_telephone ORDER BY ID_telephone ASC"""
                    mc_afficher.execute(strsql_telephone_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_telephone"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_telephone_selected_dictionnaire = {"value_ID_telephone_selected": ID_telephone_sel}
                    strsql_telephone_afficher = """SELECT * FROM t_telephone WHERE ID_telephone = %(value_ID_telephone_selected)s"""

                    mc_afficher.execute(strsql_telephone_afficher, valeur_ID_telephone_selected_dictionnaire)
                else:
                    strsql_telephone_afficher = """SELECT * FROM t_telephone ORDER BY ID_telephone DESC"""

                    mc_afficher.execute(strsql_telephone_afficher)

                data_telephone = mc_afficher.fetchall()

                print("data_telephone ", data_telephone, " Type : ", type(data_telephone))

                # Différencier les messages si la table est vide.
                if not data_telephone and ID_telephone_sel == 0:
                    flash("""La table "t_telephone" est vide. !!""", "warning")
                elif not data_telephone and ID_telephone_sel > 0:
                    # Si l'utilisateur change l'ID_telephone dans l'URL et que le genre n'existe pas,
                    flash(f"Le telephone demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_telephone" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données telephone affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. telephone_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} telephone_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("telephone/telephone_afficher_wtf.html", data=data_telephone)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /telephone_ajouter

    Test : ex : http://127.0.0.1:5005/sim_ajouter

    Paramètres : sans

    But : Ajouter un genre pour un film

    Remarque :  Dans le champ "name_pc_html" du formulaire "sim/sim_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/telephone_ajouter", methods=['GET', 'POST'])
def telephone_ajouter_wtf():
    form = FormWTFAjouterTelephone()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion telephone ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestiontelephone {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_telephone_wtf = form.nom_telephone_wtf.data
                name_telephone = name_telephone_wtf.lower()

                numero_de_serie_telephone_wtf = form.numero_de_serie_telephone_wtf.data
                numero_de_serie_telephone = numero_de_serie_telephone_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_Modele_telephone": name_telephone, "value_numero_de_serie_telephone": numero_de_serie_telephone}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_telephone = "INSERT INTO t_telephone (`ID_telephone`, `Modele_telephone`, `numero_de_serie_telephone`) VALUES (NULL,%(value_Modele_telephone)s, %(value_numero_de_serie_telephone)s)"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_telephone, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('telephone_afficher', order_by='DESC', ID_telephone_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_pc_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_pc_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_telephone_crud:
            code, msg = erreur_gest_telephone_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion telephone CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_telephone_crud.args[0]} , "
                  f"{erreur_gest_telephone_crud}", "danger")

    return render_template("telephone/telephone_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /telephone_update

    Test : ex cliquer sur le menu "sim" puis cliquer sur le bouton "EDIT" d'un "genre"

    Paramètres : sans

    But : Editer(update) un genre qui a été sélectionné dans le formulaire "telephone_afficher.html"

    Remarque :  Dans le champ "nom_telephone_update_wtf" du formulaire "sim/telephone_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/telephone_update", methods=['GET', 'POST'])
def telephone_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_telephone"
    ID_telephone_update = request.values['ID_telephone_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateTelephone()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "telephone_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_telephone_update = form_update.nom_telephone_update_wtf.data
            name_telephone_update = name_telephone_update.lower()

            numero_de_serie_telephone_update = form_update.numero_de_serie_telephone_update_wtf.data
            numero_de_serie_telephone_update = numero_de_serie_telephone_update.lower()

            valeur_update_dictionnaire = {"value_ID_telephone": ID_telephone_update, "value_name_telephone": name_telephone_update, "value_numero_de_serie_telephone": numero_de_serie_telephone_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_Modele_telephone = "UPDATE t_telephone SET Modele_telephone = %(value_name_telephone)s, numero_de_serie_telephone = %(value_numero_de_serie_telephone)s WHERE ID_telephone = %(value_ID_telephone)s"
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_Modele_telephone, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_telephone_update"
            return redirect(url_for('telephone_afficher', order_by="ASC", ID_telephone_sel=ID_telephone_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_telephone" et "Modele_PC" de la "t_telephone"
            str_sql_ID_telephone = "SELECT * FROM t_telephone WHERE ID_telephone = %(value_ID_telephone)s"
            valeur_select_dictionnaire = {"value_ID_telephone": ID_telephone_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_telephone, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_telephone = mybd_curseur.fetchone()
            print("data_nom_telephone ", data_nom_telephone, " type ", type(data_nom_telephone), " telephone ",
                  data_nom_telephone["Modele_telephone"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "telephone_update_wtf.html"
            form_update.nom_telephone_update_wtf.data = data_nom_telephone["Modele_telephone"]
            form_update.numero_de_serie_telephone_update_wtf.data = data_nom_telephone["numero_de_serie_telephone"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans telephone_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans telephone_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_telephone_crud:
        code, msg = erreur_gest_telephone_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_telephone_crud} ", "danger")
        flash(f"Erreur dans telephone_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_telephone_crud.args[0]} , "
              f"{erreur_gest_telephone_crud}", "danger")
        flash(f"__KeyError dans telephone_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("telephone/telephone_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /sim_delete

    Test : ex. cliquer sur le menu "sim" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "telephone_afficher.html"

    Remarque :  Dans le champ "nom_sim_delete_wtf" du formulaire "sim/sim_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/telephone_delete", methods=['GET', 'POST'])
def telephone_delete_wtf():
    data_sim_attribue_telephone_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_telephone"
    ID_telephone_delete = request.values['ID_telephone_btn_delete_html']

    # Objet formulaire pour effacer le PC sélectionné.
    form_delete = FormWTFDeleteTelephone()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("telephone_afficher", order_by="ASC", ID_telephone_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_sim_attribue_telephone_delete = session['data_sim_attribue_telephone_delete']
                print("data_sim_attribue_telephone_delete ", data_sim_attribue_telephone_delete)

                flash(f"Effacer le telephone de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_telephone": ID_telephone_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_sim_telephone = """DELETE FROM t_avoir_sim_telephone WHERE FK_Telephone = %(value_ID_telephone)s"""
                str_sql_delete_IDTelephone = """DELETE FROM t_telephone WHERE ID_telephone = %(value_ID_telephone)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_telephone_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_telephone_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_sim_telephone, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_IDTelephone, valeur_delete_dictionnaire)

                flash(f"Telephone définitivement effacé !!", "success")
                print(f"Telephone définitivement effacé !!")

                # afficher les données
                return redirect(url_for('telephone_afficher', order_by="ASC", ID_telephone_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_telephone": ID_telephone_delete}
            print(ID_telephone_delete, type(ID_telephone_delete))

            # Requête qui affiche tous les films qui ont le genre que l'utilisateur veut effacer
            str_sql_telephone_delete = """SELECT ID_Avoir_Sim_Telephone, Modele, ID_telephone, Modele_telephone FROM t_avoir_sim_telephone 
                                            INNER JOIN t_sim ON t_avoir_sim_telephone.FK_Sim = t_sim.ID_Sim
                                            INNER JOIN t_telephone ON t_avoir_sim_telephone.FK_Telephone = t_telephone.ID_telephone
                                            WHERE FK_Telephone = %(value_ID_telephone)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_telephone_delete, valeur_select_dictionnaire)
            data_telephone_attribue_sim_delete = mybd_curseur.fetchall()
            print("data_sim_attribue_telephone_delete...", data_sim_attribue_telephone_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_sim_attribue_telephone_delete'] = data_sim_attribue_telephone_delete

            # Opération sur la BD pour récupérer "ID_telephone" et "intitule_genre" de la "t_telephone"
            str_sql_ID_telephone = "SELECT * FROM t_telephone WHERE ID_telephone = %(value_ID_telephone)s"

            mybd_curseur.execute(str_sql_ID_telephone, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_telephone = mybd_curseur.fetchone()
            print("data_nom_telephone ", data_nom_telephone, " type ", type(data_nom_telephone), " Telephone ",
                  data_nom_telephone["Modele_telephone"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "sim_delete_wtf.html"
            form_delete.nom_telephone_delete_wtf.data = data_nom_telephone["Modele_telephone"]

            # Le bouton pour l'action "DELETE" dans le form. "sim_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans telephone_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans telephone_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_telephone_crud:
        code, msg = erreur_gest_telephone_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_telephone_crud} ", "danger")

        flash(f"Erreur dans telephone_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_telephone_crud.args[0]} , "
              f"{erreur_gest_telephone_crud}", "danger")

        flash(f"__KeyError dans telephone_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("telephone/telephone_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_sim_associes=data_sim_attribue_telephone_delete)
