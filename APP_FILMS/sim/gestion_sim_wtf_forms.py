"""
    Fichier : gestion_sim_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterSim(FlaskForm):
    """
        Dans le formulaire "sim_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_sim_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_sim_wtf = StringField("Tappe le modele de la carte sim ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_sim_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    numero_de_serie_regexp = "^[A-Z 0-9]+$"
    #numero_de_serie_wtf = StringField("Tappe le numéro de série de la sim ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                              Regexp(numero_de_serie_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    numero_de_serie_wtf = StringField("Tappe le numéro de série de la sim ")

    Date_achat_regexp = "^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$"
    Date_achat_wtf = StringField("Tappe la date d'achat de la sim ",
                                      validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                  Regexp(Date_achat_regexp,
                                                         message="Date comme yyyy-mm-dd" )
                                                  ])
    Pin1_regexp = "^[0-9]+$"
    Pin1_wtf = StringField("Tappe le Pin 1 de la sim ",
                                      validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                  Regexp(Pin1_regexp,
                                                         message="Que des chiffres sont valides")
                                                  ])
    Pin2_regexp = "^[0-9]+$"
    Pin2_wtf = StringField("Tappe le Pin 2 de la sim ",
                           validators=[Length(min=2, max=20, message="min 2 max 20"),
                                       Regexp(Pin2_regexp,
                                              message="Que des chiffres sont valides")
                                       ])
    numero_de_telephone_regexp = "^[0-9 +]+$"
    numero_de_telephone_wtf = StringField("Tappe le numéro de téléphone de la sim ",
                           validators=[Length(min=2, max=20, message="min 2 max 20"),
                                       Regexp(numero_de_telephone_regexp,
                                              message="Seulement des chiffres et le caractère + ")
                                       ])
    submit = SubmitField("Enregistrer sim")


class FormWTFUpdateSim(FlaskForm):
    """
        Dans le formulaire "sim_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_sim_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_sim_update_wtf = StringField("Tappe le modele de la carte sim ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_sim_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    numero_de_serie_update_regexp = "^[A-Z 0-9]+$"
    #numero_de_serie_update_wtf = StringField("Tappe le numéro de série de la sim ",validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                    Regexp(numero_de_serie_update_regexp,
    #                                                                        message="Pas de chiffres, de "
    #                                                                            "caractères "
    #                                                                            "spéciaux, "
    #                                                                            "d'espace à double, de double "
    #                                                                             "apostrophe, de double trait "
    #                                                                            "union")
    #                                                                    ])
    numero_de_serie_update_wtf = StringField("Tappe le numéro de série de la sim ")

    Date_achat_update_regexp = "^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$"
    Date_achat_update_wtf = StringField("Tappe la date d'achat de la sim ",validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                        Regexp(Date_achat_update_regexp,
                                                                            message="Date comme yyyy-mm-dd")
                                                                        ])
    Pin1_update_regexp = "^[0-9]+$"
    Pin1_update_wtf = StringField("Tappe le Pin 1 de la sim ",validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                        Regexp(Pin1_update_regexp,
                                                                            message="Que des chiffres sont valides")
                                                                        ])
    Pin2_update_regexp = "^[0-9]+$"
    Pin2_update_wtf = StringField("Tappe le Pin 2 de la sim ",validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                        Regexp(Pin2_update_regexp,
                                                                            message="Que des chiffres sont valides")
                                                                        ])
    numero_de_telephone_update_regexp = "^[A-Z 0-9 +]+$"
    numero_de_telephone_update_wtf = StringField("Tappe le numéro de téléphone de la sim ",validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                        Regexp(numero_de_telephone_update_regexp,
                                                                             message="Seulement des chiffres et le caractère + ")
                                                                        ])

    submit = SubmitField("Update sim")


class FormWTFDeleteSim(FlaskForm):
    """
        Dans le formulaire "sim_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_sim_delete_wtf = StringField("Effacer la carte sim")
    submit_btn_del = SubmitField("Effacer la carte sim")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
