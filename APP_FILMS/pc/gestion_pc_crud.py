"""
    Fichier : gestion_sim_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les sim.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.pc.gestion_pc_wtf_forms import FormWTFAjouterPc
from APP_FILMS.pc.gestion_pc_wtf_forms import FormWTFDeletePc
from APP_FILMS.pc.gestion_pc_wtf_forms import FormWTFUpdatePc

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /pc_afficher

    Test : ex : http://127.0.0.1:5005/pc_afficher

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_PC_sel = 0 >> tous les pc(s).
                ID_PC_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/pc_afficher/<string:order_by>/<int:ID_PC_sel>", methods=['GET', 'POST'])
def pc_afficher(order_by, ID_PC_sel):
    # def Pc_afficher(order_by, ID_PC_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion PC ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPC {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_PC_sel == 0:
                    strsql_pc_afficher = """SELECT * FROM t_pc ORDER BY ID_PC ASC"""
                    mc_afficher.execute(strsql_pc_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_pc"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_PC_selected_dictionnaire = {"value_ID_PC_selected": ID_PC_sel}
                    strsql_pc_afficher = """SELECT * FROM t_pc WHERE ID_PC = %(value_ID_PC_selected)s"""

                    mc_afficher.execute(strsql_pc_afficher, valeur_ID_PC_selected_dictionnaire)
                else:
                    strsql_pc_afficher = """SELECT * FROM t_pc ORDER BY ID_PC DESC"""

                    mc_afficher.execute(strsql_pc_afficher)

                data_pc = mc_afficher.fetchall()

                print("data_pc ", data_pc, " Type : ", type(data_pc))

                # Différencier les messages si la table est vide.
                if not data_pc and ID_PC_sel == 0:
                    flash("""La table "t_pc" est vide. !!""", "warning")
                elif not data_pc and ID_PC_sel > 0:
                    # Si l'utilisateur change l'ID_PC dans l'URL et que le genre n'existe pas,
                    flash(f"Le PC demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_pc" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données PC affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. pc_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} pc_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("pc/pc_afficher_wtf.html", data=data_pc)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /pc_ajouter

    Test : ex : http://127.0.0.1:5005/sim_ajouter

    Paramètres : sans

    But : Ajouter un genre pour un film

    Remarque :  Dans le champ "name_pc_html" du formulaire "sim/sim_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/pc_ajouter", methods=['GET', 'POST'])
def pc_ajouter_wtf():
    form = FormWTFAjouterPc()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion pc ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPc {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_pc_wtf = form.nom_pc_wtf.data
                name_pc = name_pc_wtf.lower()

                Numero_de_serie_PC_wtf = form.Numero_de_serie_PC_wtf.data
                Numero_de_serie_PC = Numero_de_serie_PC_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_Modele_PC": name_pc, "value_Numero_de_serie_PC": Numero_de_serie_PC}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_pc = "INSERT INTO t_pc (`ID_PC`, `Modele_PC`, `Numero_de_serie_PC`) VALUES (NULL,%(value_Modele_PC)s, %(value_Numero_de_serie_PC)s)"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_pc, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('pc_afficher', order_by='DESC', ID_PC_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_pc_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_pc_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_pc_crud:
            code, msg = erreur_gest_pc_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion pc CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_pc_crud.args[0]} , "
                  f"{erreur_gest_pc_crud}", "danger")

    return render_template("pc/pc_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /pc_update

    Test : ex cliquer sur le menu "sim" puis cliquer sur le bouton "EDIT" d'un "genre"

    Paramètres : sans

    But : Editer(update) un genre qui a été sélectionné dans le formulaire "pc_afficher.html"

    Remarque :  Dans le champ "nom_pc_update_wtf" du formulaire "sim/pc_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/pc_update", methods=['GET', 'POST'])
def pc_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_PC"
    ID_PC_update = request.values['ID_PC_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatePc()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "pc_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_pc_update = form_update.nom_pc_update_wtf.data
            name_pc_update = name_pc_update.lower()

            Numero_de_serie_PC_update = form_update.Numero_de_serie_PC_update_wtf.data
            Numero_de_serie_PC_update = Numero_de_serie_PC_update.lower()

            valeur_update_dictionnaire = {"value_ID_PC": ID_PC_update, "value_name_pc": name_pc_update, "value_Numero_de_serie_PC": Numero_de_serie_PC_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_Modele_PC = "UPDATE t_pc SET Modele_PC = %(value_name_pc)s, Numero_de_serie_PC = %(value_Numero_de_serie_PC)s WHERE ID_PC = %(value_ID_PC)s"
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_Modele_PC, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_PC_update"
            return redirect(url_for('pc_afficher', order_by="ASC", ID_PC_sel=ID_PC_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_PC" et "Modele_PC" de la "t_pc"
            str_sql_ID_PC = "SELECT * FROM t_pc WHERE ID_PC = %(value_ID_PC)s"
            valeur_select_dictionnaire = {"value_ID_PC": ID_PC_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_PC, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_pc = mybd_curseur.fetchone()
            print("data_nom_pc ", data_nom_pc, " type ", type(data_nom_pc), " PC ",
                  data_nom_pc["Modele_PC"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "pc_update_wtf.html"
            form_update.nom_pc_update_wtf.data = data_nom_pc["Modele_PC"]
            form_update.Numero_de_serie_PC_update_wtf.data = data_nom_pc["Numero_de_serie_PC"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pc_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pc_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_pc_crud:
        code, msg = erreur_gest_pc_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_pc_crud} ", "danger")
        flash(f"Erreur dans pc_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_pc_crud.args[0]} , "
              f"{erreur_gest_pc_crud}", "danger")
        flash(f"__KeyError dans pc_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("pc/pc_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /sim_delete

    Test : ex. cliquer sur le menu "sim" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "pc_afficher.html"

    Remarque :  Dans le champ "nom_sim_delete_wtf" du formulaire "sim/sim_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/pc_delete", methods=['GET', 'POST'])
def pc_delete_wtf():
    data_sim_attribue_pc_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_PC"
    ID_PC_delete = request.values['ID_PC_btn_delete_html']

    # Objet formulaire pour effacer le PC sélectionné.
    form_delete = FormWTFDeletePc()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("pc_afficher", order_by="ASC", ID_PC_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_sim_attribue_pc_delete = session['data_sim_attribue_pc_delete']
                print("data_sim_attribue_pc_delete ", data_sim_attribue_pc_delete)

                flash(f"Effacer le PC de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_PC": ID_PC_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_sim_pc = """DELETE FROM t_avoir_sim_pc WHERE FK_PC = %(value_ID_PC)s"""
                str_sql_delete_IDPc = """DELETE FROM t_pc WHERE ID_PC = %(value_ID_PC)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_pc_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_pc_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_sim_pc, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_IDPc, valeur_delete_dictionnaire)

                flash(f"Pc définitivement effacé !!", "success")
                print(f"PC définitivement effacé !!")

                # afficher les données
                return redirect(url_for('pc_afficher', order_by="ASC", ID_PC_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_PC": ID_PC_delete}
            print(ID_PC_delete, type(ID_PC_delete))

            # Requête qui affiche tous les films qui ont le genre que l'utilisateur veut effacer
            str_sql_pc_delete = """SELECT ID_Avoir_Sim_PC, Modele, ID_PC, Modele_PC FROM t_avoir_sim_pc 
                                            INNER JOIN t_sim ON t_avoir_sim_pc.FK_Sim = t_sim.ID_Sim
                                            INNER JOIN t_pc ON t_avoir_sim_pc.FK_PC = t_pc.ID_PC
                                            WHERE FK_PC = %(value_ID_PC)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_pc_delete, valeur_select_dictionnaire)
            data_pc_attribue_sim_delete = mybd_curseur.fetchall()
            print("data_sim_attribue_pc_delete...", data_sim_attribue_pc_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_sim_attribue_pc_delete'] = data_sim_attribue_pc_delete

            # Opération sur la BD pour récupérer "ID_PC" et "intitule_genre" de la "t_pc"
            str_sql_ID_PC = "SELECT * FROM t_pc WHERE ID_PC = %(value_ID_PC)s"

            mybd_curseur.execute(str_sql_ID_PC, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_pc = mybd_curseur.fetchone()
            print("data_nom_pc ", data_nom_pc, " type ", type(data_nom_pc), " PC ",
                  data_nom_pc["Modele_PC"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "sim_delete_wtf.html"
            form_delete.nom_pc_delete_wtf.data = data_nom_pc["Modele_PC"]

            # Le bouton pour l'action "DELETE" dans le form. "sim_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pc_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pc_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_pc_crud:
        code, msg = erreur_gest_pc_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_pc_crud} ", "danger")

        flash(f"Erreur dans pc_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_pc_crud.args[0]} , "
              f"{erreur_gest_pc_crud}", "danger")

        flash(f"__KeyError dans pc_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("pc/pc_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_sim_associes=data_sim_attribue_pc_delete)
