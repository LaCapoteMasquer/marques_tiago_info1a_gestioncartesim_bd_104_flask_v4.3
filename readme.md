Module 104 2021.06.12
---


# Faire fonctionner la base de données
* Démarré le serveur mySQL.
  * Pour faire cela faut juste installer UwAmp ou xampp ou autre chose qui permettrai de démarré un serveur mySQL.
* Une fois installé
  * Pour UwAmp faut le lancez et cliquez sur le bouton "PHPMyAdmin"
  * Pour Xampp faut le lancez et cliquez sur le bouton "Start" de la ligne "Apache" et "MySQL".
  Une fois fait cliquez sur "admin" de la ligne "MySQL".
* Vous voila sur la page d'accueil du PHPMyAdmin
  * Connectez-vous en mettant l'identifiant et le mot de passe, Si vous n'avez changer rien de tout ça.
  Les identifiants par défauts seront "root" pour utilisateur et "root" pour le mot de passe. (Pour Xampp pas besoin de mot de passe)
* Dirigez-vous maintenant dans le fichier ".env" 
  * Saisissez les informations de votre serveur mySQL.
  * Si vous avez changer les identifiants n'oubliez pas de les changer, sans ça la base de données ne pourra
  pas se crée dans le PHPMyAdmin.
* Dirigez-vous dans le fichier "1_ImportationDumpSql.py", qui se trouve
  dans "APP_FILMS", ensuite "zzzdemos", faite un clique droit et lancé le "run",
  Cela va crée la base de données dans le PHPMyAdmin
* Rafraîchissez la page internet ou il y a le PHPMyAdmin et vérifié que la base de données soit bien introduite.
* Une fois la base de données dans le PHPMyAdmin, dirigez-vous dans le fichier "1_run_serveur_flask.py" faites un
  clique droit et lancé le run. cliquez sur le lien qui vous sera proposé en bas dans les logs du python.
* Vous aurez ainsi accès au site pour la base de données et reste plus qu'a bosser.  
  

