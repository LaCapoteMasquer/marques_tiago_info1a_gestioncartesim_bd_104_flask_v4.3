"""
    Fichier : gestion_sim_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterTelephone(FlaskForm):
    """
        Dans le formulaire "sim_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_telephone_regexp = "^[A-Z 0-9]+$"
    #nom_pc_wtf = StringField("Tappe le modele du PC ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(nom_pc_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    nom_telephone_wtf = StringField("Tappe le modele du Téléphone ")

    numero_de_serie_telephone_regexp = "^[A-Z 0-9]+$"
    #Numero_de_serie_PC_wtf = StringField("Tappe le numéro de série du PC ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(Numero_de_serie_PC_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    numero_de_serie_telephone_wtf = StringField("Tappe le numéro de série du Téléphone ")
    submit = SubmitField("Enregistrer Téléphone")


class FormWTFUpdateTelephone\
            (FlaskForm):
    """
        Dans le formulaire "sim_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_telephone_update_regexp = "^[A-Z 0-9]+$"
    #nom_pc_update_wtf = StringField("Tappe le modele du PC ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(nom_pc_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                     "spéciaux, "
    #                                                                                     "d'espace à double, de double "
    #                                                                                     "apostrophe, de double trait "
    #                                                                                     "union")
    #                                                                      ])
    nom_telephone_update_wtf = StringField("Tappe le modele du Téléphone ")

    numero_de_serie_telephone_update_regexp = "^[A-Z 0-9]+$"
    #Numero_de_serie_PC_update_wtf = StringField("Tappe le numéro de série du PC ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(Numero_de_serie_PC_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                    "spéciaux, "
    #                                                                                    "d'espace à double, de double "
    #                                                                                    "apostrophe, de double trait "
    #                                                                                     "union")
    #
    #                                                                     ])
    numero_de_serie_telephone_update_wtf = StringField("Tappe le numéro de série du Téléphone ")
    submit = SubmitField("Update Téléphone")


class FormWTFDeleteTelephone(FlaskForm):
    """
        Dans le formulaire "sim_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_telephone_delete_wtf = StringField("Effacer le Téléphone")
    submit_btn_del = SubmitField("Effacer le Téléphone")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
