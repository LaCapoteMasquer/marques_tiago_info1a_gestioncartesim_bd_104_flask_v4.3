"""
    Fichier : gestion_sim_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les sim.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.sim.gestion_sim_wtf_forms import FormWTFAjouterSim
from APP_FILMS.sim.gestion_sim_wtf_forms import FormWTFDeleteSim
from APP_FILMS.sim.gestion_sim_wtf_forms import FormWTFUpdateSim

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /sim_afficher
    
    Test : ex : http://127.0.0.1:5005/sim_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_Sim_sel = 0 >> tous les sim.
                ID_Sim_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/sim_afficher/<string:order_by>/<int:ID_Sim_sel>", methods=['GET', 'POST'])
def sim_afficher(order_by, ID_Sim_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion sim ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionSim {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_Sim_sel == 0:
                    strsql_sim_afficher = """SELECT * FROM t_sim ORDER BY ID_Sim ASC"""
                    mc_afficher.execute(strsql_sim_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_sim"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_ID_Sim_selected_dictionnaire = {"value_ID_Sim_selected": ID_Sim_sel}
                    strsql_sim_afficher = """SELECT * FROM t_sim WHERE ID_Sim = %(value_ID_Sim_selected)s"""

                    mc_afficher.execute(strsql_sim_afficher, valeur_ID_Sim_selected_dictionnaire)
                else:
                    strsql_sim_afficher = """SELECT * FROM t_sim ORDER BY ID_Sim DESC"""

                    mc_afficher.execute(strsql_sim_afficher)

                data_sim = mc_afficher.fetchall()

                print("data_sim ", data_sim, " Type : ", type(data_sim))

                # Différencier les messages si la table est vide.
                if not data_sim and ID_Sim_sel == 0:
                    flash("""La table "t_sim" est vide. !!""", "warning")
                elif not data_sim and ID_Sim_sel > 0:
                    # Si l'utilisateur change l'ID_Sim dans l'URL et que le genre n'existe pas,
                    flash(f"La carte sim demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_sim" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données sim affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. sim_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} sim_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("sim/sim_afficher_wtf.html", data=data_sim)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /sim_ajouter
    
    Test : ex : http://127.0.0.1:5005/sim_ajouter
    
    Paramètres : sans
    
    But : Ajouter un genre pour un film
    
    Remarque :  Dans le champ "name_sim_html" du formulaire "sim/sim_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/sim_ajouter", methods=['GET', 'POST'])
def sim_ajouter_wtf():
    form = FormWTFAjouterSim()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion sim ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionSim {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_sim_wtf = form.nom_sim_wtf.data
                name_sim = name_sim_wtf.lower()

                numero_de_serie_wtf = form.numero_de_serie_wtf.data
                numero_de_serie = numero_de_serie_wtf.lower()

                Date_achat_wtf = form.Date_achat_wtf.data
                Date_achat = Date_achat_wtf.lower()

                Pin1_wtf = form.Pin1_wtf.data
                Pin1 = Pin1_wtf.lower()

                Pin2_wtf = form.Pin2_wtf.data
                Pin2 = Pin2_wtf.lower()

                numero_de_telephone_wtf = form.numero_de_telephone_wtf.data
                numero_de_telephone = numero_de_telephone_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_Modele": name_sim, "value_numero_de_serie": numero_de_serie, "value_Date_achat": Date_achat, "value_Pin1": Pin1, "value_Pin2": Pin2, "value_numero_de_telephone": numero_de_telephone}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                # strsql_insert_sim = """INSERT INTO t_sim (ID_Sim, Modele, numero_de_serie)
                #                     VALUES (NULL,%(value_Modele)s, %(value_numero_de_serie)s)"""
                strsql_insert_sim = "INSERT INTO `t_sim` (`ID_Sim`, `Modele`, `numero_de_serie`, `Date_achat`, `Pin1`, `Pin2`, `numero_de_telephone`) VALUES (NULL,%(value_Modele)s, %(value_numero_de_serie)s, %(value_Date_achat)s, %(value_Pin1)s, %(value_Pin2)s, %(value_numero_de_telephone)s) "


                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_sim, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('sim_afficher', order_by='DESC', ID_Sim_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_sim_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_sim_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_sim_crud:
            code, msg = erreur_gest_sim_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion sim CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_sim_crud.args[0]} , "
                  f"{erreur_gest_sim_crud}", "danger")

    return render_template("sim/sim_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /sim_update
    
    Test : ex cliquer sur le menu "sim" puis cliquer sur le bouton "EDIT" d'un "genre"
    
    Paramètres : sans
    
    But : Editer(update) un genre qui a été sélectionné dans le formulaire "pc_afficher_wtf.html"
    
    Remarque :  Dans le champ "nom_sim_update_wtf" du formulaire "sim/sim_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/sim_update_wtf", methods=['GET', 'POST'])
def sim_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_Sim"
    ID_Sim_update = request.values['ID_Sim_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateSim()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "sim_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_sim_update = form_update.nom_sim_update_wtf.data
            name_sim_update = name_sim_update.lower()

            numero_de_serie_update = form_update.numero_de_serie_update_wtf.data
            numero_de_serie_update = numero_de_serie_update.lower()

            Date_achat_update = form_update.Date_achat_update_wtf.data
            Date_achat_update = Date_achat_update.lower()

            Pin1_update = form_update.Pin1_update_wtf.data
            Pin1_update = Pin1_update.lower()

            Pin2_update = form_update.Pin2_update_wtf.data
            Pin2_update = Pin2_update.lower()

            numero_de_telephone_update = form_update.numero_de_telephone_update_wtf.data
            numero_de_telephone_update = numero_de_telephone_update.lower()

            valeur_update_dictionnaire = {"value_ID_Sim": ID_Sim_update, "value_name_sim": name_sim_update, "value_numero_de_serie": numero_de_serie_update, "value_Date_achat": Date_achat_update, "value_Pin1": Pin1_update, "value_Pin2": Pin2_update, "value_numero_de_telephone": numero_de_telephone_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_Modele = "UPDATE t_sim SET Modele = %(value_name_sim)s, numero_de_serie = %(value_numero_de_serie)s, Date_achat = %(value_Date_achat)s, Pin1 = %(value_Pin1)s,  Pin2 = %(value_Pin2)s,  numero_de_telephone = %(value_numero_de_telephone)s  WHERE ID_Sim = %(value_ID_Sim)s"
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_Modele, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_Sim_update"
            return redirect(url_for('sim_afficher', order_by="ASC", ID_Sim_sel=ID_Sim_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_Sim" et "Modele" de la "t_sim"
            str_sql_ID_Sim = "SELECT * FROM t_sim WHERE ID_Sim = %(value_ID_Sim)s"
            valeur_select_dictionnaire = {"value_ID_Sim": ID_Sim_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_Sim, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_sim = mybd_curseur.fetchone()

            print("data_nom_sim ", data_nom_sim, " type ", type(data_nom_sim), " sim ",
                  data_nom_sim["Modele"])



            # Afficher la valeur sélectionnée dans le champ du formulaire "sim_update_wtf.html"
            form_update.nom_sim_update_wtf.data = data_nom_sim["Modele"]
            form_update.numero_de_serie_update_wtf.data = data_nom_sim["numero_de_serie"]
            form_update.Date_achat_update_wtf.data = data_nom_sim["Date_achat"]
            form_update.Pin1_update_wtf.data = data_nom_sim["Pin1"]
            form_update.Pin2_update_wtf.data = data_nom_sim["Pin2"]
            form_update.numero_de_telephone_update_wtf.data = data_nom_sim["numero_de_telephone"]



    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans sim_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans sim_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_sim_crud:
        code, msg = erreur_gest_sim_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_sim_crud} ", "danger")
        flash(f"Erreur dans sim_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_sim_crud.args[0]} , "
              f"{erreur_gest_sim_crud}", "danger")
        flash(f"__KeyError dans sim_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("sim/sim_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /sim_delete
    
    Test : ex. cliquer sur le menu "sim" puis cliquer sur le bouton "DELETE" d'un "genre"
    
    Paramètres : sans
    
    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "pc_afficher_wtf.html"
    
    Remarque :  Dans le champ "nom_sim_delete_wtf" du formulaire "sim/sim_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/sim_delete", methods=['GET', 'POST'])
def sim_delete_wtf():
    data_pc_attribue_sim_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_Sim"
    ID_Sim_delete = request.values['ID_Sim_btn_delete_html']

    # Objet formulaire pour effacer la sim sélectionné.
    form_delete = FormWTFDeleteSim()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("sim_afficher", order_by="ASC", ID_Sim_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_pc_attribue_sim_delete = session['data_pc_attribue_sim_delete']
                print("data_pc_attribue_sim_delete ", data_pc_attribue_sim_delete)

                flash(f"Effacer la carte sim de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_Sim": ID_Sim_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_pc_sim = """DELETE FROM t_avoir_sim_pc WHERE FK_Sim = %(value_ID_Sim)s"""
                str_sql_delete_IDSim = """DELETE FROM t_sim WHERE ID_Sim = %(value_ID_Sim)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_sim_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_sim_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_pc_sim, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_IDSim, valeur_delete_dictionnaire)

                flash(f"sim définitivement effacé !!", "success")
                print(f"sim définitivement effacé !!")

                # afficher les données
                return redirect(url_for('sim_afficher', order_by="ASC", ID_Sim_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_Sim": ID_Sim_delete}
            print(ID_Sim_delete, type(ID_Sim_delete))

            # Requête qui affiche tous les films qui ont le genre que l'utilisateur veut effacer
            str_sql_sim_delete = """SELECT ID_Avoir_Sim_PC, Modele_PC, ID_Sim, Modele FROM t_avoir_sim_pc 
                                            INNER JOIN t_pc ON t_avoir_sim_pc.FK_PC = t_PC.ID_PC
                                            INNER JOIN t_sim ON t_avoir_sim_pc.FK_Sim = t_sim.ID_Sim
                                            WHERE FK_Sim = %(value_ID_Sim)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_sim_delete, valeur_select_dictionnaire)
            data_pc_attribue_sim_delete = mybd_curseur.fetchall()
            print("data_pc_attribue_sim_delete...", data_pc_attribue_sim_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_pc_attribue_sim_delete'] = data_pc_attribue_sim_delete

            # Opération sur la BD pour récupérer "ID_Sim" et "intitule_genre" de la "t_sim"
            str_sql_ID_Sim = "SELECT * FROM t_sim WHERE ID_Sim = %(value_ID_Sim)s"

            mybd_curseur.execute(str_sql_ID_Sim, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_sim = mybd_curseur.fetchone()
            print("data_nom_sim ", data_nom_sim, " type ", type(data_nom_sim), " sim ",
                  data_nom_sim["Modele"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "sim_delete_wtf.html"
            form_delete.nom_sim_delete_wtf.data = data_nom_sim["Modele"]

            # Le bouton pour l'action "DELETE" dans le form. "sim_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans sim_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans sim_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_sim_crud:
        code, msg = erreur_gest_sim_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_sim_crud} ", "danger")

        flash(f"Erreur dans sim_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_sim_crud.args[0]} , "
              f"{erreur_gest_sim_crud}", "danger")

        flash(f"__KeyError dans sim_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("sim/sim_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_pc_associes=data_pc_attribue_sim_delete)
